package com.example.deveshnee.googleplaces.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.deveshnee.googleplaces.R;
import com.example.deveshnee.googleplaces.client.GooglePlacesClient;
import com.example.deveshnee.googleplaces.client.PlaceFoundInterface;
import com.example.deveshnee.googleplaces.places.MyPlace;
import com.example.deveshnee.googleplaces.places.PlaceAdapter;

import java.util.List;

public class PlaceActivity extends AppCompatActivity implements PlaceFoundInterface{

    RecyclerView recyclerView;
    private List<MyPlace> placeList;
    private PlaceAdapter placeAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_main);
        GooglePlacesClient googleApiClient = new GooglePlacesClient(this);
        recyclerView = findViewById(R.id.place_recycler_view);
        placeList = googleApiClient.getNearbyPlaces();
    }

    private void initialiseCardRecycler() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        placeAdapter = new PlaceAdapter(this, placeList);
        recyclerView.setAdapter(placeAdapter);
    }

    @Override
    public void onPlacesFound() {
        initialiseCardRecycler();
    }

    @Override
    public void onImagesLoaded(String placeId, Bitmap placePhoto) {
        for (int i = 0; i < placeList.size(); i++) {
            if (placeList.get(i).getId().equals(placeId)) {
                placeList.get(i).setIcon(placePhoto);
            }
        }
        placeAdapter.notifyDataSetChanged();
        recyclerView.invalidate();
    }
}
