package com.example.deveshnee.googleplaces.tiles;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.deveshnee.googleplaces.R;

import java.util.List;


public class TileGridViewAdapter extends BaseAdapter {

    private Context context;
    private final List<Tile> icons;

    public TileGridViewAdapter(Context context, List<Tile> icons) {
        this.context = context;
        this.icons = icons;
    }

    @Override
    public int getCount() {
        return icons.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View gridview;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {

            gridview = new View(context);
            if (inflater != null) {
                gridview = inflater.inflate(R.layout.image_tile, viewGroup);
                ImageView imageView = gridview.findViewById(R.id.image_view);
                TextView attributionsTextView = gridview.findViewById(R.id.attributions_text);

                attributionsTextView.setText(icons.get(i).getAttributions());
                imageView.setImageBitmap(icons.get(i).getPlaceImage());
            }
        }
        else {
            gridview = view;
        }
        return gridview;
    }
}
