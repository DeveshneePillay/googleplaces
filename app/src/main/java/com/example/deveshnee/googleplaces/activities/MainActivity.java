package com.example.deveshnee.googleplaces.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.deveshnee.googleplaces.R;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity {

    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private static final int ENABLE_LOCATION_REQUEST = 2;
    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        setupImageButton();
    }

    private void setupImageButton() {
        ImageView imageButton = findViewById(R.id.image_button);
        Animation shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shakeanimation);
        imageButton.setAnimation(shakeAnimation);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLastKnownLocation();
            }
        });
    }

    private void getLastKnownLocation() {
        if (locationManager != null) {
            if (permissionsGranted() && locationSettingsEnabled()) {
                goToPlacesNearMe();
            }
        }
    }

    private void goToPlacesNearMe() {
        Intent i = new Intent(this, PlaceActivity.class);
        startActivity(i);
    }

    private boolean permissionsGranted() {
        boolean permissions = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!permissions) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_REQUEST);
        }
        return permissions;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if (grantResults.length >= 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (locationSettingsEnabled()) {
                    getLastKnownLocation();
                }
            }
        }
    }

    private boolean locationSettingsEnabled() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LocationRequest locationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(10 * 1000)
                    .setFastestInterval(10 * 1000);

            LocationSettingsRequest.Builder settingBuilder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            settingBuilder.setAlwaysShow(true);

            Task<LocationSettingsResponse> responseTask = LocationServices.getSettingsClient(this)
                    .checkLocationSettings(settingBuilder.build());

            responseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
                @Override
                public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                    try {
                        task.getResult(ApiException.class);
                    } catch (ApiException e) {
                        switch (e.getStatusCode()) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    ResolvableApiException resolvableApiException = (ResolvableApiException) e;
                                    resolvableApiException.startResolutionForResult(MainActivity.this, ENABLE_LOCATION_REQUEST);
                                } catch (IntentSender.SendIntentException ie) {
                                    ie.printStackTrace();
                                }
                        }
                    }
                }
            });
        }
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ENABLE_LOCATION_REQUEST) {
            if (permissionsGranted()) {
                goToPlacesNearMe();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
