package com.example.deveshnee.googleplaces.client;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.deveshnee.googleplaces.R;
import com.example.deveshnee.googleplaces.activities.PlaceActivity;
import com.example.deveshnee.googleplaces.places.MyPlace;
import com.example.deveshnee.googleplaces.tiles.Tile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResponse;
import com.google.android.gms.location.places.PlacePhotoResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GooglePlacesClient implements com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener {

    private GeoDataClient geoDataClient;
    private PlaceDetectionClient placeDetectionClient;
    private Context context;
    private List<MyPlace> placeList = new ArrayList<>();
    private PlaceFoundInterface placeCallBack;
    private Bitmap placePhoto;
    private Map<String, List<Tile>> placePhotos = new HashMap<>();

    public GooglePlacesClient(Context context) {
        this.context = context;
        initialise();
        findNearbyPlaces();
    }

    private void initialise() {
        geoDataClient = Places.getGeoDataClient(context);
        placeDetectionClient = Places.getPlaceDetectionClient(context);
        placeCallBack = (PlaceActivity) context;
        placePhoto = BitmapFactory.decodeResource(context.getResources(), R.mipmap.no_place_image);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(context, "Unable to estabalish connection", Toast.LENGTH_SHORT).show();
    }

    private void findNearbyPlaces() {
        try {
            Task<PlaceLikelihoodBufferResponse> placeResult = placeDetectionClient.getCurrentPlace(null);
            placeResult.addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
                @Override
                public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
                    PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
                    for (PlaceLikelihood place : likelyPlaces) {
                        Place currentPlace = place.getPlace();
                        if (currentPlace != null) {
                            getPlacePhoto(currentPlace.getId());
                            if (placePhoto != null) {
                                addNewPlace(currentPlace.getId(), currentPlace.getName(), currentPlace.getAddress(), currentPlace.getRating(), currentPlace.getPhoneNumber(), placePhoto);
                            }
                        }
                    }
                    likelyPlaces.release();
                    placeCallBack.onPlacesFound();
                }
            });
        } catch (SecurityException e) {
            Log.d("", e.getMessage());
        }
    }

    //added checks for when data is missing from api's
    private void addNewPlace(CharSequence id, CharSequence name, CharSequence address, float rating, CharSequence phoneNumber, Bitmap placePhoto) {
        String placeId = String.valueOf(id) != null ? String.valueOf(id) : "";
        String placeName = String.valueOf(name) != null ? String.valueOf(name) : "";
        String placeAddress = String.valueOf(address) != null ? String.valueOf(address) : "";
        String placeRating = Float.toString(rating) != null ? Float.toString(rating) : "";
        String placeContact = String.valueOf(phoneNumber) != null ? String.valueOf(phoneNumber) : "";
        Log.d("PlaceFound", placeId + "\t" + placeName + "\t" + placeAddress + "\t" + placeRating + "\t" + placeContact);
        placeList.add(new MyPlace(placeId, placeName, placeAddress, placeRating, placeContact, placePhoto));
    }

    public List<MyPlace> getNearbyPlaces() {
        return placeList;
    }

    private void getPlacePhoto(final String placeId) {
        final Task<PlacePhotoMetadataResponse> photoMetadataResponse = geoDataClient.getPlacePhotos(placeId);
        photoMetadataResponse.addOnCompleteListener(new OnCompleteListener<PlacePhotoMetadataResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlacePhotoMetadataResponse> task) {

                PlacePhotoMetadataResponse photos = task.getResult();
                PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();

                if (photoMetadataBuffer != null) {
                    for (PlacePhotoMetadata photoMetadata : photoMetadataBuffer) {

                        final CharSequence attributions = photoMetadata.getAttributions();

                        Task<PlacePhotoResponse> photoResponse = geoDataClient.getPhoto(photoMetadata);
                        photoResponse.addOnCompleteListener(new OnCompleteListener<PlacePhotoResponse>() {
                            @Override
                            public void onComplete(@NonNull Task<PlacePhotoResponse> task) {
                                PlacePhotoResponse photo = task.getResult();
                                if (photo != null) {
                                    if (photo.getBitmap() != null) {
                                        if (!placePhotos.containsKey(placeId)) {
                                            List<Tile> placeBitmaps = new ArrayList<>();
                                            placeBitmaps.add(new Tile(attributions, photo.getBitmap()));
                                            placePhotos.put(placeId, placeBitmaps);
                                        } else {
                                            placePhotos.get(placeId).add(new Tile(attributions, photo.getBitmap()));
                                        }
                                        placeCallBack.onImagesLoaded(placeId, photo.getBitmap());
                                        for (int i = 0; i < placeList.size(); i++) {
                                            if (placeList.get(i).getId().equals(placeId)) {
                                                placeList.get(i).setAllPhotos(placePhotos);
                                                Log.d("Added to map", ""+ placeId + "\t" + placePhotos.size());
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                    photoMetadataBuffer.release();
                }
            }
        });

    }
}