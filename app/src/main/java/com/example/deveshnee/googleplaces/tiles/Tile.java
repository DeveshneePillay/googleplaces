package com.example.deveshnee.googleplaces.tiles;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Tile implements Parcelable {

    private String attributions;
    private Bitmap placeImage;

    public Tile (CharSequence attributions, Bitmap placeImage){
        this.attributions = attributions.toString();
        this.placeImage =placeImage;
    }

    private Tile(Parcel in) {
        attributions = in.readString();
        placeImage = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<Tile> CREATOR = new Creator<Tile>() {
        @Override
        public Tile createFromParcel(Parcel in) {
            return new Tile(in);
        }

        @Override
        public Tile[] newArray(int size) {
            return new Tile[size];
        }
    };

    public String getAttributions() {
        return attributions;
    }

    public Bitmap getPlaceImage() {
        return placeImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(attributions);
        parcel.writeParcelable(placeImage, i);
    }
}
