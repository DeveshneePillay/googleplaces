package com.example.deveshnee.googleplaces.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.deveshnee.googleplaces.R;

public class ImageFullScreeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_full_screen);

        ImageView imageView = findViewById(R.id.image_full);
        TextView textView = findViewById(R.id.text_view);

        Intent photoData = getIntent();
        Bitmap bitmap = photoData.getParcelableExtra(String.valueOf(R.string.placephoto));
        String text = photoData.getStringExtra(String.valueOf(R.string.photoattributions));

        textView.setText(text);
        imageView.setImageBitmap(bitmap);

        setupView();
    }

    private void setupView(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay();

        int width = (int) (displayMetrics.widthPixels * 0.8);
        int height = (int) (displayMetrics.widthPixels * 0.8);

        getWindow().setLayout(width, height);
    }
}
