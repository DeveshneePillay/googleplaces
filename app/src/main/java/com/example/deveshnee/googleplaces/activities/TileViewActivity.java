package com.example.deveshnee.googleplaces.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.deveshnee.googleplaces.R;
import com.example.deveshnee.googleplaces.places.MyPlace;
import com.example.deveshnee.googleplaces.tiles.Tile;
import com.example.deveshnee.googleplaces.tiles.TileGridViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class TileViewActivity extends AppCompatActivity {

    private List<Tile> placeTiles = new ArrayList<>();
    private MyPlace placeSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tile_grid);

        Intent placeData = getIntent();
        placeSelected = placeData.getParcelableExtra("placeselected");
        //placeTiles = placeData.getParcelableArrayListExtra("placephotos");

        initialise();
    }

    private void initialise() {

        if (placeSelected != null) {
//            List<Tile> placeImages = placeSelected.getAllPlacePhotosMap().get(placeSelected.getId());
//           if (placeImages.size() > 0) {
//                placeTiles.addAll(placeImages);
//            }
        }

        TileGridViewAdapter gridViewAdapter = new TileGridViewAdapter(this, placeTiles);
        GridView gridView = findViewById(R.id.grid_view);
        gridView.setAdapter(gridViewAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Tile item = (Tile) parent.getItemAtPosition(position);

                Intent intent = new Intent(TileViewActivity.this, ImageFullScreeActivity.class);
                intent.putExtra(String.valueOf(R.string.placephoto), item.getPlaceImage());
                intent.putExtra(String.valueOf(R.string.photoattributions), item.getAttributions());

                startActivity(intent);
            }
        });
    }
}
