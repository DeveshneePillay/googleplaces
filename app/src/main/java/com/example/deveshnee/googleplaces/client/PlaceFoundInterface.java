package com.example.deveshnee.googleplaces.client;

import android.graphics.Bitmap;


public interface PlaceFoundInterface {
   void onPlacesFound();
   void onImagesLoaded(String placeId, Bitmap placePhoto);
}
