package com.example.deveshnee.googleplaces.places;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.deveshnee.googleplaces.tiles.Tile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyPlace implements Parcelable {

    private String id;
    private String name;
    private String address;
    private String contact;
    private String rating;
    private Bitmap icon;
    private Map<String, List<Tile>> allPlacePhotos = new HashMap<>();

    public MyPlace(String id, String name, String address, String rating, String contact, Bitmap icon) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.rating = rating;
        this.contact = contact;
        this.icon = icon;
    }

    private MyPlace(Parcel in) {
        id = in.readString();
        name = in.readString();
        address = in.readString();
        rating = in.readString();
        contact = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    String getAddress() {
        return address;
    }

    String getRating() {
        return String.valueOf(rating);
    }

    String getContact() {
        return contact;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public void setAllPhotos (Map<String, List<Tile>> placePhotos){
        this.allPlacePhotos =placePhotos;
    }
    
    public static final Creator<MyPlace> CREATOR = new Creator<MyPlace>() {
        @Override
        public MyPlace createFromParcel(Parcel in) {
            return new MyPlace(in);
        }

        @Override
        public MyPlace[] newArray(int size) {
            return new MyPlace[size];
        }
    };

    Map<String, List<Tile>> getAllPlacePhotosMap() {
        return allPlacePhotos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(address);
        parcel.writeString(contact);
        parcel.writeString(rating);
    }
}
