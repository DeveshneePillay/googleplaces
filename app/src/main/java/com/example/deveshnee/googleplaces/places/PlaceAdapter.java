package com.example.deveshnee.googleplaces.places;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.deveshnee.googleplaces.R;
import com.example.deveshnee.googleplaces.activities.PlaceActivity;
import com.example.deveshnee.googleplaces.activities.TileViewActivity;
import com.example.deveshnee.googleplaces.tiles.Tile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder> {

    private Context context;
    private List<MyPlace> placeList;
    private List<Tile> placeTiles;

    public PlaceAdapter(Context context, List<MyPlace> placeList) {
        this.context = context;
        this.placeList = placeList;
    }

    @Override
    public PlaceAdapter.PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.place_card, parent, false);
        return new PlaceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlaceAdapter.PlaceViewHolder holder, final int position) {

        final MyPlace place = placeList.get(position);
        holder.placeName.setText(place.getName());
        holder.placeAddress.setText(place.getAddress());
        holder.placeContact.setText((place.getContact()));
        holder.placeRating.setText(place.getRating());
        holder.imageView.setImageBitmap(place.getIcon());
        holder.actionMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewMore = new Intent(context, TileViewActivity.class);
                MyPlace selectedPlace = placeList.get(holder.getAdapterPosition());
                if (selectedPlace != null){
                    placeTiles = selectedPlace.getAllPlacePhotosMap().get(selectedPlace.getId());
                }
                viewMore.putExtra("placeselected", selectedPlace);
                //viewMore.putParcelableArrayListExtra("placephotos", placeTiles);
                context.startActivity(viewMore);
            }
        });
    }

    @Override
    public int getItemCount() {
        return placeList.size();
    }

    class PlaceViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView placeName, placeAddress, placeContact, placeRating;
        Button actionMoreButton, actionMapButton;

        PlaceViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view);
            placeName = itemView.findViewById(R.id.place_name);
            placeAddress = itemView.findViewById(R.id.place_address);
            placeRating = itemView.findViewById(R.id.place_rating);
            placeContact = itemView.findViewById(R.id.place_contact);
            actionMoreButton = itemView.findViewById(R.id.action_button_more);
            actionMapButton = itemView.findViewById(R.id.action_button_map);
        }
    }
}
